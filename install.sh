#!/bin/bash

## Install the packages
yaourt -Syy --noconfirm $(cat ./packages.list);

## Copy the files
cp -Rlv ./etc/* /etc/;
cp -Rlv ./usr/* /usr/;

## Enable the services
systemctl enable nginx.service nginx-cgi.service  php-fpm.service;

## Start the services
systemctl start nginx.service nginx-cgi.service php-fpm.service;

## Notify the user
echo "NOTE:  Standalone Installation requires manual modification of files in /etc/nginx and /etc/nginx/conf.d";

